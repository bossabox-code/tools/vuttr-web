import React, { useState, useEffect } from "react";

import Tool from "./Tool";
import ModalAdd from "./ModalAdd";
import ModalRemove from "./ModalRemove";

import "./App.scss";

export default function Tools() {
  const [tools, setTools] = useState([]);
  const [tool, setTool] = useState({});
  const [filteredTools, setFilteredTools] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [isTagsOnly, setIsTagsOnly] = useState(false);
  const [modalRemoveOpen, setModalRemoveOpen] = useState(false);
  const [modalAddOpen, setModalAddOpen] = useState(false);

  useEffect(() => {
    const savedTools = JSON.parse(localStorage.getItem("tools")) || [
      {
        title: "Notion",
        link: "https://www.notion.so/",
        description:
          "All-in-one workspace. One tool for your whole team. Write, plan, and get organized.",
        tags: "organization, planning, collaboration, writing, calendar",
      },
      {
        title: "JSON Server",
        link: "https://github.com/typicode/json-server",
        description:
          "Get a full fake REST API with zero coding in less than 30 seconds (seriously).",
        tags: "api, json, schema, node, github, rest",
      },
      {
        title: "Fastify",
        link: "https://www.fastify.io/",
        description: "Fast and low overhead web framework, for Node.js.",
        tags: "web, framework, node, http2",
      },
    ];

    if (savedTools.length) {
      setTools(savedTools);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("tools", JSON.stringify(tools));

    setFilteredTools(tools);
  }, [tools]);

  const handleSearchTextChange = (event) => {
    let newSearchText = event.target.value;
    setSearchText(newSearchText);

    handleToolsFilter({ tools, searchText: newSearchText, isTagsOnly });
  };

  const handleIsTagsOnlyChange = (event) => {
    let newIsTagsOnly = event.target.checked;
    setIsTagsOnly(newIsTagsOnly);

    handleToolsFilter({ tools, searchText, isTagsOnly: newIsTagsOnly });
  };

  const handleToolsFilter = ({ tools, searchText, isTagsOnly }) => {
    let newFilteredTools = tools.filter(
      (tool) =>
        (!isTagsOnly &&
          tool.title.toLowerCase().search(searchText.toLowerCase()) !== -1) ||
        JSON.stringify(tool.tags)
          .toLowerCase()
          .search(searchText.toLowerCase()) !== -1
    );

    setFilteredTools(newFilteredTools);
  };

  const handleAddClick = () => {
    setModalAddOpen(true);
  };

  const handleModalAddConfirm = (tool) => {
    setTools([...tools, tool]);
    setModalAddOpen(false);
  };

  const handleRemoveClick = (tool) => {
    setTool(tool);
    setModalRemoveOpen(true);
  };

  const handleModalRemoveConfirm = (tool) => {
    setTools(tools.filter((t) => t.title !== tool.title));
    setModalRemoveOpen(false);
  };

  return (
    <div className="container">
      <header className="App-header my-5">
        <h1 className="font-weight-bold">VUTTR</h1>
        <h2 className="text-muted">Very Useful Tools to Remember</h2>
      </header>
      <h3 className="font-weight-bold">Tools</h3>
      <div className="form-row">
        <div className="col-4">
          <input
            type="text"
            className="form-control"
            placeholder="search"
            value={searchText}
            onChange={(e) => {
              handleSearchTextChange(e);
            }}
          />
        </div>

        <div className="col-4">
          <label className="form-check mt-2">
            <input
              type="checkbox"
              className="form-check-input"
              value={isTagsOnly}
              onChange={(e) => handleIsTagsOnlyChange(e)}
            />
            search in tags only
          </label>
        </div>

        <div className="col-2 offset-2">
          <button
            type="button"
            className="btn btn-primary btn-block"
            onClick={(e) => handleAddClick()}
          >
            Add
          </button>
        </div>
      </div>

      <ModalAdd
        modalOpen={modalAddOpen}
        onConfirm={(tool) => handleModalAddConfirm(tool)}
        onCancel={() => setModalAddOpen(false)}
      />

      <ModalRemove
        tool={tool}
        modalOpen={modalRemoveOpen}
        onConfirm={(tool) => handleModalRemoveConfirm(tool)}
        onCancel={() => setModalRemoveOpen(false)}
      />

      {filteredTools.length > 0 &&
        filteredTools.map((tool, index) => (
          <Tool
            key={tool.title + index}
            tool={tool}
            index={index}
            onRemoveClick={(tool) => handleRemoveClick(tool)}
          />
        ))}
    </div>
  );
}
